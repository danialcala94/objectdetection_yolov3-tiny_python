# -*- coding: utf-8 -*-
"""
Created on Sun May 12 20:37:30 2019

@author: Daniel
"""

def write(file, text):
    file = open(file, "a") 
 
    file.write(text)
     
    file.close() 
    
def empty(file):
    file = open(file, "w")
    file.write("")
    file.close()