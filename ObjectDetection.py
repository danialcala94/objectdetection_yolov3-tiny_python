# -*- coding: utf-8 -*-
"""
@author: ALCALÁ VALERA, DANIEL
"""

# import required packages
import cv2
import numpy as np
import os
import logger
import glob
import datetime
import json
import math
    
VALID_RESULTS_FILE = "totest/counting.people"
DEBUG_LOG_FILE = "debug.log"
RESULTS_FILE = "results/people.results"
YOLO_CLASSES = "coco.names"
YOLO_WEIGHTS = "yolov3-tiny.weights"
YOLO_CONFIG = "yolov3-tiny.cfg"

number_of_images = 0
number_of_people_detected = 0
number_of_people_to_detect = 0
success_list = []

show_only_once = 1
    
# Getting output_layers name
def get_output_layers(net):
    layer_names = net.getLayerNames()
    
    if show_only_once == 1:
        print("***********************")
        print("ALL YOLOv3-tiny LAYERS:")
        for l in layer_names:
            print(l)
    
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    
    if show_only_once == 1:
        print("YOLOv3-tiny OUTPUT LAYERS ONLY:")
        for ol in output_layers:
            print(ol)
        print("*************************")

    return output_layers

RECTANGLE_BORDER_SIZE = 2
SHOW_LABEL = 0
FONT_SIZE = 1
FONT_FAMILY = cv2.FONT_HERSHEY_SIMPLEX

# function to draw bounding box on the detected object with class name
def draw_bounding_box(img, class_id, confidence, x, y, x_plus_w, y_plus_h, color=(18, 238, 137)):

    cv2.rectangle(img, (x, y), (x_plus_w, y_plus_h), color, RECTANGLE_BORDER_SIZE)

    if SHOW_LABEL == 1:
        label = str(classes[class_id])
        cv2.putText(img, label, (x + 5, y + 20), FONT_FAMILY, 0.50, color, FONT_SIZE)
    
def delete_previous_results(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)
    
def realbbox_coords(bbox):
    return [bbox[0][0], bbox[0][1], bbox[1][0], bbox[1][1]]
    
# Upper_left X - Upper_left Y - Down_right X - Down_right Y
def bbox_coords(bbox):
    x = bbox[0]
    y = bbox[1]
    w = bbox[2]
    h = bbox[3]

    return [x, y, x + w, y + h]

EPSILON = 1e-5
# Gives the intersection over the union of
def iou(boxA, boxB):
    
    # COORDINATES OF THE INTERSECTION BOX
    x1 = max(boxA[0], boxB[0])
    y1 = max(boxA[1], boxB[1])
    x2 = min(boxA[2], boxB[2])
    y2 = min(boxA[3], boxB[3])

    # AREA OF OVERLAP - Area where the boxes intersect
    width = (x2 - x1)
    height = (y2 - y1)
    # handle case where there is NO overlap
    if (width<0) or (height <0):
        return 0.0
    area_overlap = width * height

    # COMBINED AREA
    area_a = (boxA[2] - boxA[0]) * (boxA[3] - boxA[1])
    area_b = (boxB[2] - boxB[0]) * (boxB[3] - boxB[1])
    area_combined = area_a + area_b - area_overlap

    # RATIO OF AREA OF OVERLAP OVER COMBINED AREA
    iou = area_overlap / (area_combined + EPSILON)
    return iou

# This would not be used anymore. Enhanced with real bounding boxes labeling.
def get_people_number_in_image(json_filename):
    return len(get_bounding_boxes_from_json_file)

def get_bounding_boxes_from_json_file(json_filename):
    #Read JSON data into the datastore variable
    with open(json_filename, encoding='utf-8') as f:
        datastore = json.loads(f.read())
    
    all_bounding_boxes = []
    for bbox in datastore["shapes"]:
        all_bounding_boxes.append(bbox["points"]);
    
    return all_bounding_boxes
    
#########################

logger.empty(DEBUG_LOG_FILE)
logger.empty(RESULTS_FILE)
delete_previous_results('results/')
logger.write(DEBUG_LOG_FILE, "STARTING ANALYSIS AT " + str(datetime.datetime.now()) + "\n\n\n")


for filename in glob.glob('totest/*.jpg'):
    #global number_of_images
    number_of_images += 1
    people_detected = 0
    people_to_detect = 0
    
    

    image_path = filename.replace("\\", "/")
    logger.write(RESULTS_FILE, ">>> " + os.path.basename(image_path) + " <<<\n")
    print("   >>> " + image_path + " <<<   ")
    logger.write(DEBUG_LOG_FILE, "Analyzing " + os.path.basename(image_path) + "\n")
    
    # Getting real bounding boxes for our image
    json_boundingboxes_file = filename.replace("\\", "/").replace(".jpg", ".json")
    print("Reading existing bounding boxes for " + filename)
    
    real_bounding_boxes = get_bounding_boxes_from_json_file(json_boundingboxes_file)
    people_to_detect = len(real_bounding_boxes)
    number_of_people_to_detect += people_to_detect
    print('There are ' + str(people_to_detect) + ' real people in this image.')
    
    # read input image
    image = cv2.imread(image_path)
    image_copy = image.copy()

    Width = image.shape[1]
    Height = image.shape[0]
    scale = 0.00392
    IMAGE_WIDTH = 416
    IMAGE_HEIGHT = 416

    # Read class names from '*.names'
    classes = None
    with open(YOLO_CLASSES, 'r') as f:
        classes = [line.strip() for line in f.readlines()]

    # Build the net (with '*.weights' and '*.cfg')
    net = cv2.dnn.readNet(YOLO_WEIGHTS, YOLO_CONFIG)

    # Input as blob
    blob = cv2.dnn.blobFromImage(image, scale, (IMAGE_WIDTH, IMAGE_HEIGHT), (0, 0, 0), True, crop = False)

    # Set the network input
    net.setInput(blob)

    # run inference through the network
    # and gather predictions from output layers
    # This is the main processing
    outs = net.forward(get_output_layers(net))

    # initialization
    class_ids = []
    confidences = []
    boxes = []
    # The maximun IoU accepted
    nms_threshold = 0.1
    CONFIDENCE_THRESHOLD = 0.2
    HIT_THRESHOLD = 0.2
    
    num_of_hits = 0
    mean_confidence = 0.0
    
    # for each detection from each output layer 
    # get the confidence, class id, bounding box params
    # ignoring detections under HIT_THRESHOLD
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            #logger.write(DEBUG_LOG_FILE, "Confidence: " + str(confidence))
            
            # Only consider class 0 (person)
            if class_id == 0:
                if confidence > CONFIDENCE_THRESHOLD:
                    num_of_hits += 1
                    mean_confidence += confidence
                    center_x = int(detection[0] * Width)
                    center_y = int(detection[1] * Height)
                    w = int(detection[2] * Width)
                    h = int(detection[3] * Height)
                    x = center_x - w / 2
                    y = center_y - h / 2
                    class_ids.append(class_id)
                    confidences.append(float(confidence))
                    boxes.append([x, y, w, h])
                    # message = "   [DETECTION hit > " + str(HIT_THRESHOLD) + "] with confidence = " + str(confidence) + "."
                    # print(message)
                    # logger.write(RESULTS_FILE, message + "\n")
            
    if num_of_hits != 0:
        mean_confidence = mean_confidence * 100 / num_of_hits
    # message = "Mean confidence of = " + str(mean_confidence) + " on " + str(num_of_hits) + " hits."
    # print(message)
    # logger.write(RESULTS_FILE, message + "\n")
    
    # apply non-max suppression
    indices = cv2.dnn.NMSBoxes(boxes, confidences, HIT_THRESHOLD, nms_threshold)
    
    # go through the detections remaining
    # after nms and draw bounding box
    for i in indices:
        i = i[0]
        box = boxes[i]
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        # message = "Bounding box [x = " + str(x) + ", y = " + str(y) + ", w = " + str(w) + ", h = " + str(h) + "]"
        # print(message)
        # logger.write(RESULTS_FILE, message + "\n")
        draw_bounding_box(image, class_ids[i], confidences[i], round(x), round(y), round(x + w), round(y + h))
    

    # This is the previous way of analyzing, just with people number.
    #people_to_detect = get_people_number_in_image(int(os.path.basename(image_path).replace(".jpg", "")))
    logger.write(DEBUG_LOG_FILE, "Must be " + str(people_to_detect) + " people and has been detected " + str(people_detected) + ".\n")
    
    # [GeneratedBbox, RealBbox, iou]
    hit_bboxes = []

    # Detecting TP
    for box in boxes:
        stop_this = 0
        # Improve it by choosing only the best hit
        for realbox in real_bounding_boxes:
            iou_value = iou(bbox_coords(box), realbbox_coords(realbox))
            if (iou_value > HIT_THRESHOLD):
                hit_bboxes.append([bbox_coords(box), realbbox_coords(realbox), iou_value])
                print("   >>> TP : " + str(iou_value))
                stop_this = 1
            if (stop_this == 1):
                break
            
    # Removing duplicities
    best_boxes = []

    j = 0

    for hit_bbox in hit_bboxes:
        #print(hit_bboxes[i])
        found = False
        for box in best_boxes:
            if box[1] == hit_bbox[1]:
                found = True
                break
        if not found:
            best_boxes.append(hit_bbox)
            # Choose the best option
            while j < len(best_boxes):
            #for hit_bbox_aux in hit_bboxes:
                if best_boxes[j][1] == box[1]:
                    if (best_boxes[i][2] < box[2]):
                        best_boxes[i] = box
                j += 1
        

    
    # We set the best bounding boxes again in the main array
    # Comment this to see failing bboxes
    hit_bboxes = best_boxes
    
    number_of_people_detected += len(hit_bboxes)
    
     # GENERATING SOME STATS
    TP = len(hit_bboxes)
    FP = len(boxes) - TP
    #TN = 
    FN = len(real_bounding_boxes) - TP
    if (TP + FP) == 0:
        Precission = 0
    else:
        Precission = TP / (TP + FP)
    if (TP + FN) == 0:
        Recall = 0
    else: 
        Recall = TP / (TP + FN)
    Accuracy = TP / (TP + FP + FN)
    
    # Writing stats
    logger.write(RESULTS_FILE, "People to be detected: " + str(len(real_bounding_boxes)) + "\n")
    logger.write(RESULTS_FILE, "True Positives: " + str(TP) + "\n")
    logger.write(RESULTS_FILE, "False Positives: " + str(FP) + "\n")
    logger.write(RESULTS_FILE, "False Negatives: " + str(FN) + "\n")
    logger.write(RESULTS_FILE, "PRECISSION: " + str(Precission) + "\n")
    logger.write(RESULTS_FILE, "RECALL: " + str(Recall) + "\n")
    logger.write(RESULTS_FILE, "ACCURACY: " + str(Accuracy) + "\n")
    
     # save output image to disk
    cv2.imwrite("results/" + os.path.basename(image_path), image)
    logger.write(DEBUG_LOG_FILE, "---------------------\n")
    
    if show_only_once == 1:
        show_only_once = 0
           
    # GENERATING OUTPUT IMAGES
    image_copy_all = image_copy.copy()
    image_copy_original = image_copy.copy()
    
    # Exporting image with hit bboxes and respecting real bboxes
    for box in hit_bboxes:
        # Drawing detected bounding boxes in blue
        draw_bounding_box(image_copy, 0, 1, (int)(box[0][0]), (int)(box[0][1]), (int)(box[0][2]), (int)(box[0][3]))
        # Drawing real bounding boxes in green
        draw_bounding_box(image_copy, 0, 1, (int)(box[1][0]), (int)(box[1][1]), (int)(box[1][2]), (int)(box[1][3]), (200, 0, 0))
    cv2.imwrite("results/" + filename.replace("\\", "/").replace(".jpg", "_hit_bboxes.jpg").replace("totest/", ""), image_copy)
    for box in hit_bboxes:
        # Drawing detected bounding boxes in blue
        draw_bounding_box(image_copy_all, 0, 1, (int)(box[0][0]), (int)(box[0][1]), (int)(box[0][2]), (int)(box[0][3]))
    
    # Exporting image with all bboxes
    my_all_bboxes = []
    for box in real_bounding_boxes:
        my_all_bboxes.append(realbbox_coords(box))
    for box in my_all_bboxes:
        draw_bounding_box(image_copy_all, 0, 1, (int)(box[0]), (int)(box[1]), (int)(box[2]), (int)(box[3]), (200, 0, 0))
    cv2.imwrite("results/" + filename.replace("\\", "/").replace(".jpg", "_all_bboxes.jpg").replace("totest/", ""), image_copy_all)
    
    # Exporting image with only real bboxes
    for box in my_all_bboxes:
        draw_bounding_box(image_copy_original, 0, 1, (int)(box[0]), (int)(box[1]), (int)(box[2]), (int)(box[3]), (200, 0, 0))
    cv2.imwrite("results/" + filename.replace("\\", "/").replace(".jpg", "_real_bboxes.jpg").replace("totest/", ""), image_copy_original)
    
    # release resources
    #cv2.destroyAllWindows()
success_sum = 0
for su in success_list:
    success_sum += su
success_mean = success_sum / number_of_images

print("Detected " + str(number_of_people_detected) + " from " + str(number_of_people_to_detect) + " existing, in " + str(number_of_images) + " images.")
logger.write(RESULTS_FILE, "Detected " + str(number_of_people_detected) + " from " + str(number_of_people_to_detect) + " existing, in " + str(number_of_images) + " images.")
success_mean_based_on_people = number_of_people_detected * 100 / number_of_people_to_detect
logger.write(RESULTS_FILE, "\n\nMean Success: " + str(success_mean) + "\nSuccess based on people amount: " + str(success_mean_based_on_people))

